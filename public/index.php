<!DOCTYPE html>
<!--
    Copyright (C) 2023 Gustavo Calvo

    groff-web is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    groff-web is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar. If not, see <https://www.gnu.org/licenses/>.
-->
<html>
<?php
    exec("mkdir -p gen && rm -rf gen/*");
    $uid = bin2hex(random_bytes(5));

    # Configuration
    $groff = "/usr/bin/groff";
    $cmd = "$groff -ketpG -mspdf -Tpdf gen/$uid.ms > gen/$uid.pdf";
    # In case you want to use an external script
    #$cmd = "sh groff-compiler.sh";

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['codebox'])) {
        $code = $_POST['codebox'];
        file_put_contents("gen/$uid.ms", $code);
        exec("$cmd");
    }
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Minimal web front-end to the GNU roff typesetting program.">
    <title>Groff Web</title>
    <style>
        body,pre,.button {
            background-color: #121212;
            color: #928374;
            font-family: monospace;
            text-align: center;
            border-radius: 10px;
            border-width: 1px;
            padding: 10px 30px;
            margin: 10px auto;
        }
        textarea,pre,.button {
            border: 1px solid #3a3e41;
        }
        .button:hover {
            border-color: #928374;
            color: #fe8019;
        }
        strong,a {
            background-color: #121212;
            color: #fe8019;
        }
        textarea,object,.columns {
            border-radius: 10px;
            border-width: 1px;
            max-width: 1300px;
            margin: 10px auto;
            width: 100%;
            columns: 2;
        }
        @media only screen and (max-width: 800px) {
            .columns {
                columns: 1;
                width: 90%;
            }
        }
        /* codebox */
        textarea {
            background-color: #282828;
            text-align: left;
            color: #83a598;
            height: 734px;
        }
        /* pdf */
        object {
            height: 800px;
        }
        pre {
            text-align: left;
            white-space: pre-wrap;
            font-family: monospace;
        }
        textarea:focus {
            outline: none !important;
            border:1px solid #928374;
        }
    </style>
</head>
<body>
    <header><p>
        <strong>Groff Web</strong> | <?php echo exec("$groff -v | head -n 1")?>
    </p> </header>
    <div class="columns">
        <form method="POST">
            <div><input type="submit" value="Compile" class="button"></div>
            <textarea name="codebox"><?= htmlspecialchars(file_get_contents("gen/$uid.ms")) ?></textarea>
        </form>
        <object type="application/pdf" data="/gen/<?php echo $uid?>.pdf?#&navpanes=0">
            <div><button class="button"><a href="/gen/<?php echo $uid?>.pdf">Download PDF</a></div>
            <div>
                <pre><code>
.\" Template
.\" .ds FAM T       \" H=Helvetica C=Courier
.\" .nr PS 10p      \" Point size
.\" .nr VS 12p      \" Line spacing
.
.TL
title
.AU
author
.AI
institution
.
.AB
abstract
.AE
.
.NH
.XN "numbered heading"
.
.PP
indented \f[CW]paragraph\f[] \m[blue]with\m[] \f[B]some\f[] \f[I]formatting\f[]
.
.\" .TC
                </code></pre>
            </div>
        </object>
    </div>
    <footer><p>
        Copyright (C) 2023 Gustavo Calvo |
        <a href="https://gitlab.com/tavo-wasd/groff-web" target="_blank" rel="noopener noreferrer">Source</a> |
        <a href="https://www.gnu.org/licenses/gpl-3.0.txt" target="_blank" rel="noopener noreferrer">GPL-3.0</a>
    </p></footer>
</body>
</html>
