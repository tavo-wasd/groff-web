# groff-web

## Description

Minimal groff web frontend

![sample](sample.gif)

## Try it out

Dependencies: `groff-base grap php-fpm`.
Run the following and visit http://0.0.0.0:8000 in your browser. (It will create a ./gen directory!)

```shell
wget "https://gitlab.com/tavo-wasd/groff-web/-/raw/main/public/index.php"
php -S 0.0.0.0:8000
```

## Installation

Dependencies:

```shell
apt install -y groff-base grap nginx php-fpm
```

Download `index.php` to webroot and set up
permissions to `www-data` user.

```shell
mkdir -p /var/www/groff-web
wget -O /var/www/groff-web/index.php "https://gitlab.com/tavo-wasd/groff-web/-/raw/main/public/index.php"
chown -R www-data:www-data /var/www/groff-web
```

nginx setup.

```shell
wget -O /etc/nginx/sites-available/groff-web.conf "https://gitlab.com/tavo-wasd/groff-web/-/raw/main/nginx.conf"
sed -i 's/groff.example.org/MY.DOMAIN.COM/' /etc/nginx/sites-available/groff-web.conf
ln -s /etc/nginx/sites-available/groff-web.conf /etc/nginx/sites-enabled/
systemctl reload nginx
```

## Remove

```shell
rm -r /var/www/groff-web
rm /etc/nginx/sites-available/groff-web.conf
rm /etc/nginx/sites-enabled/groff-web.conf
systemctl reload nginx
```

## Configuration

Go to `/var/www/groff-web/index.php`, here you can:
- Change groff's PATH, for example if you installed groff in `/usr/local/bin/groff`
- Modify the groff compiler command.
- Or use an external script for compilation.

```php
<?php
    ...
    # groff settings
    $groff = "/usr/bin/groff";
    $comp = "$groff -ketpG -mspdf -Tpdf tmp/$uid.ms > tmp/$uid.pdf";
    # In case you want to use an external script to compile with groff:
    #$comp = "sh groff-compiler.sh";
    ...
?>
```

## License
GPL v3.0
